import React from 'react'
import './footer.css'

import imgLogo from '../../../assets/img/money.png'

const footer = () => {
    return (
        <footer>
            <div className="projectName">
                Débitos - Gerenciamento
            </div>
            <div className="copyRight"></div>
            <img src={imgLogo} alt="" srcset=""/>
        </footer>

    )
}

export default footer
