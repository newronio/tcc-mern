import React, { useState } from 'react'
import { saveToken } from '../../config/auth'
import './login.css'
//import { useHistory } from 'react-router-dom';
import { authentication } from '../../services/auth';
import { clientHttp } from '../../config/config';

const Login = (props) => {
    
    const [auth, setAuth] = useState({})

    //const history = useHistory()

    const handleChange = (event) => {
        setAuth({
            ...auth,
            [event.target.name]: event.target.value
        })
        return;
    }

    const isValidSubmit = () => auth.email && auth.password


    const submitLogin = async () => {
        if (isValidSubmit()) {
            const { data: { token } } = await authentication(auth)
            clientHttp.defaults.headers['x-auth-token'] = token;
            saveToken(token)
            window.location.assign('/debt-list')

        }
        return;
    }

    return (
        <section>

            <div id="login">
                <div className="form_login">
                    <div>
                        <label htmlFor="auth_login">Email</label>
                        <input type="email" id="email" name="email" onChange={handleChange} value={auth.email || ""} placeholder="Insira seu e-mail" />
                    </div>
                    <div>
                        <label htmlFor="auth_password">Senha</label>
                        <input type="password" id="password" name="password" onChange={handleChange} value={auth.password || ""} placeholder="Insira a sua senha" />
                    </div>
                    <button disabled={!isValidSubmit()} onClick={submitLogin}>Entrar</button> 
                </div>
            </div>
        </section>

    )
}

export default Login
