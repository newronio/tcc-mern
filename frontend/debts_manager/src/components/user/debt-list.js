import React, { useEffect, useState } from 'react'
import { ListDebt, DeleteDebt } from '../../services/debts'
import './debt.css'
import Loading from '../loading/loading'
import Nav from '../../components/layout/nav/nav'
import{ useHistory } from 'react-router-dom'

const DebtList = () => {
    const [ debts, setDebts ] = useState([])
    const history = useHistory()

    const getList = async () => {
       try{

        const debts = await ListDebt()
        setDebts(debts.data)

       }catch(err){           
        console.log('error', err)
       }
    } 

    const editDebt = (debt) => history.push(`/edit/${debt._id}`)
    
    const deleteDebt = async ({_id, typeDebt}) => {
        //console.log(_id)
        try{
            if(window.confirm(`Você deseja excluir o débito: ${typeDebt}`)){
                await DeleteDebt(_id)
                getList()
            }
           
        }catch(error){
            console.log(error)
        }
    }

    const verifyIsEmpty = debts.length === 0

    const mountTable = () => {

        const rows = debts.map((debt, index) => ( 
                <tr key={index}>
                    <td>{debt.typeDebt}</td>
                    <td>{debt.debtLocation}</td>
                    <td>{debt.debtCost}</td>
                    <td>{debt.date}</td>
                    <td>
                        <span onClick={() => editDebt(debt)}>Editar</span> |
                        <span onClick={() => deleteDebt(debt)}>Excluir</span>
                    </td>
                </tr>
            )) 

            return !verifyIsEmpty ? (
                <table border="1">
                        <thead>
                            <tr>
                                <th>TIPO</th>
                                <th>LOCALIZAÇÃO</th>
                                <th>CUSTO</th>
                                <th>DATA</th>
                                <th>ALTERAÇÕES</th>
                            </tr>
                        </thead>
                    <tbody>
                        {rows}
                    </tbody>
                 </table>
            ) : ""
        
    }
        useEffect(() => {
            getList()
        }, [])

    return (
        <React.Fragment >
            <Nav name="Novo" to="/create-debt"/>
                <div className="list_debt">
                    <Loading show={verifyIsEmpty}/>
                        {mountTable()}
                </div>
        </React.Fragment>
    )
}

export default DebtList