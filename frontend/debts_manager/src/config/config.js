import axios from 'axios'
import { getToken } from './auth'

const clientHttp = axios.create({
    baseURL: 'https://debt-app.herokuapp.com/'
})

clientHttp.defaults.headers['Content-Type'] = 'application/json'

if(getToken()){
    clientHttp.defaults.headers['x-auth-token'] = getToken()
}

export {
    clientHttp
}