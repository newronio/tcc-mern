import { clientHttp } from '../config/config.js'

const CreateDebt = (data) => clientHttp.post('/debts/', data)
const ListDebt = () => clientHttp.get(`/debts`)
const DeleteDebt = (_id) => clientHttp.delete(`/debts/${_id}`)
const EditDebt = (_id) => clientHttp.post(`/debts/${_id}`)

export {
    CreateDebt,
    ListDebt,
    DeleteDebt,
    EditDebt
}