const express = require('express');
let Debt = require('../models/debt.model');
const { check, validationResult } = require('express-validator');
const app = express();
const auth = require('../middleaware/auth')
const multer = require('multer')

app.get('/', auth, async (req, res, next) => {

    try{
        const debt = await Debt.find()
        res.json(debt)
        
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server Error"})
    }
})

app.get('/:id', auth, async (req, res, next) => {

    try{
        const id = req.params.id
        
        const debtId =  await Debt.findById(id);
        res.send(debtId)

    }catch(err){
        res.status(500).send({"error": "Server Error"})
    }
})



app.post('/', auth,
[
    check('typeDebt', 'O Campo precisa ser preenchido').not().isEmpty(),
    check('typeDebt', 'Apenas texto é permitido').not().isNumeric(),
    check('debtLocation', 'O Campo precisa ser preenchido').not().isEmpty(),
    check('debtLocation', 'Apenas texto é permitido').not().isNumeric(),
    check('debtCost', 'O Campo precisa ser preenchido').not().isEmpty(),
    check('debtCost', 'Apenas números são permitidos').isNumeric(),
    check('date', 'Selecione uma data').not().isEmpty(),
],  async (req, res) => {

    try{
        let { typeDebt, debtLocation, debtCost, date} = req.body       
         
        const debts = new Debt({
            typeDebt, 
            debtLocation, 
            debtCost, 
            date
        })
            
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }
            await debts.save()
            res.json(debts)
            console.log(debts)
        
        

    }catch(error){ 
        console.error(error.message)
        res.status(500).send({"error": "Server Error"})
    }
})

app.post ('/:id', auth, [
    check('typeDebt', 'Preencha o tipo de débito').not().isEmpty(),
    check('debtLocation', 'Preencha a localização do débito').not().isEmpty(),
    check('debtCost', 'Preencha o valor do débito').not().isEmpty(),
    check('date', 'Selecione uma data').not().isEmpty(),
],  async (req, res) => {

    try{
        
        const id = req.params.id
        
        let bodyRequest = req.body

        const update = { $set: bodyRequest}

        const debt = await Debt.findByIdAndUpdate (id, update, {new: true})

        if (!debt){
            res.status(404).send({"error": "debt not exist"})
        }
            res.json(debt)
        
    }catch(error){ 
        console.error(error.message)
        res.status(500).send({"error": "Server Error"})
    }
})

app.delete('/:id', auth, async (req, res, next) => {

    try{
        const id = req.params.id
        const debt = await Debt.findByIdAndDelete(id)

        if(debt){
            res.json(debt)
        }

        res.status(500).send({"error": "Debt already deleted"})
        
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "Server Error"})
    }
})


module.exports = app;