const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const userSchema = new Schema({
   
    username : {
        type: String,
        //unique: true,
        // required: true,
        minlength: 5
    },
    name : {
        type: String,
        required: true
    },
    lastname : {
        type: String,
        // required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        bcrypt: true,
        minlength: 5,
        select: false
    },
    is_active:{
        type: Boolean,
        default: true
    },
    is_admin:{
        type: Boolean,
        default: true
    },
    date: {
        type: Date,
        default: Date.now
    }, 
}, {
    timestamps: true,
})

module.exports = mongoose.model('users', userSchema);